package sheridan;

public class Celsius {
	
	/**
	 * 
	 * @param value
	 * @return the celsius degree 
	 * converted from the fahrenhiet degree
	 */
	public static int fromFahrenheit ( int value ) throws NumberFormatException {
		
		// Minimun F is -459F
		if (value < -459) {
			throw new NumberFormatException();
		}
		
		// Initialize celsius degree
		double doubleCelsius = 0;
		
		// Add 0.5 to round up
		doubleCelsius = ((value - 32) * 5.0 / 9.0) + 0.5;
		
		int celsius = (int)doubleCelsius;
		
		return celsius;
	} 
	
}
