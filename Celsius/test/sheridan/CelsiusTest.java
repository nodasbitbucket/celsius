package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {
	/*
	 * Shinnosuke Noda
	 */
	@Test
	public void testFromFahrenheitRegular() {
		// 32F is 0C
		int f = 32;
		int c = 0;
		assertTrue("Invalid value", Celsius.fromFahrenheit(f) == c);
	}
	
	/**
	 * It cannot be lower than 0K, 459F
	 */
	@Test(expected = NumberFormatException.class)
	public void testFromFahrenheitExceptional() {
		assertFalse("Invalid value", Celsius.fromFahrenheit(-460) == 237);
	}
	
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int f = 100;
		int c = 38;
		assertTrue("Invalid value", Celsius.fromFahrenheit(f) == c);
	}
	
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int f = 100;
		int c = 37;
		assertFalse("Invalid value", Celsius.fromFahrenheit(f) == c);
	}
}
